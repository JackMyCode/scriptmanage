package com.iflytek.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONSerializer;

import com.iflytek.dao.ServerDao;
import com.iflytek.model.Server;

public class GetServerInf extends HttpServlet {

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
       // System.out.println(request.getParameter("area"));
		request.setCharacterEncoding("utf-8");
		response.setContentType("application/json;charset=utf-8");
		
		PrintWriter out = response.getWriter();
		
		String  area = request.getParameter("area");
		
		int type = 0 ;
		if("hf"==area || area.equals("hf")){
			type = 1;
		}else if(area=="bj" || area.equals("bj") ){
			type = 2;
		}else if(area== "gz" || area.equals("gz") ){
			type = 3; 
		}else if( area == "all" || area.equals("all")){
			type = 4 ;
		}	
		
		ArrayList<Server> serverList = (ArrayList<Server>) new ServerDao().selectArea(type);
		JSONArray jsarry=new JSONArray();
		String tempStr = JSONSerializer.toJSON(serverList).toString();
		
	    jsarry=JSONArray.fromObject(serverList);  
	   // System.out.println(tempStr);
	    out.write(tempStr);
		
	}

}
