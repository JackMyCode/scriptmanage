package com.iflytek.tools;

import com.iflytek.dao.ScriptDao;
import com.iflytek.model.Script;

public class AsynchScriptContent {

	public String getScriptContent(Script object){
		String content = "";
		ScriptDao scriptDao = new ScriptDao();
		
		Script script = scriptDao.selectPrecise(object);
		
		//System.out.println(script.getIp()+"|"+ script.getUser().getAccount()+"|"+  script.getUser().getPasswd());
		
		ExecuteTools exe = new ExecuteTools(script.getIp(), script.getUser().getAccount(), script.getUser().getPasswd());
		try {
			content = exe.catScript(script.getDir(),script.getScriptName(),script.getType());
			
			script.setContent(content);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return content;
	}
}
