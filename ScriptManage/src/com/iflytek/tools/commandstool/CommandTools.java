package com.iflytek.tools.commandstool;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import ch.ethz.ssh2.ChannelCondition;
import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.Session;
import ch.ethz.ssh2.StreamGobbler;

public class CommandTools {
	
	private Connection conn;
	private String ip;
	private String usr;
	private String psword;
	
	private String charset = Charset.defaultCharset().toString();
	private static final int TIME_OUT = 1000;
	
	public CommandTools(String ip, String usr, String ps) {
		this.ip = ip;
		this.usr = usr;
		this.psword =ps;
	}
	
	private boolean login() throws IOException {
		conn = new Connection(ip);
		conn.connect();
		return conn.authenticateWithPassword(usr, psword);
	}
	
	public String exec(String cmds) throws Exception {
		InputStream stdOut = null;
		InputStream stdErr = null;
		String outStr = "";
		String outErr = "";
		int ret = -1;
		try {
			if (login()) {
			// Open a new {@link Session} on this connection
			Session session = conn.openSession();
			// Execute a command on the remote machine.
			session.execCommand(cmds);
			
			stdOut = new StreamGobbler(session.getStdout());
			outStr = processStream(stdOut, charset);
			
			stdErr = new StreamGobbler(session.getStderr());
			outErr = processStream(stdErr, charset);
			session.waitForCondition(ChannelCondition.EXIT_STATUS, TIME_OUT);
			
			
			ret = session.getExitStatus();
			//System.out.println(outErr+"***********>"+ret);
			//System.out.println(outStr+"***********>"+ret);
			} else {
				return "Authenticationfailure";
			}
		} finally {
			if (conn != null) {
			conn.close();
			}
		}
		if( ret != 0 ){
			return "error" ;
		}else{
			return outStr;
		}
	}
	
	private String processStream(InputStream in, String charset) throws Exception {
		byte[] buf = new byte[1024];
		StringBuilder sb = new StringBuilder();
		while (in.read(buf) != -1) {
		sb.append(new String(buf, charset));
		}
		return sb.toString();
	}
	
	
	public String readScript(String dir , String scriptName){
		String ret ="" ;
        try    
        {  
            Connection conn = new Connection(this.ip);     
            conn.connect();     
            boolean isAuthenticated = conn.authenticateWithPassword(this.usr, this.psword);     
    
            if (isAuthenticated == false)     
                throw new IOException("Authentication failed.");     
            Session sess = conn.openSession();     
            sess.execCommand("cat "+dir+"/"+scriptName);     
            InputStream stdout = new StreamGobbler(sess.getStdout());     
            BufferedReader br = new BufferedReader(new InputStreamReader(stdout));     
            
            while (true)     
            {     
                String line = br.readLine();     
                if (line == null)     
                    break;     
                ret = ret + line +"\n";
            }    
            
            sess.close();     
            conn.close();     
        }     
        catch (IOException e)     
        {     
            e.printStackTrace(System.err);     
            System.exit(2);     
        }
        return ret;
	}
	
	public static void main(String args[]) throws Exception {
		//CommandTools exe = new CommandTools("172.16.33.71", "cqaupdate", "cqaupdate");
		CommandTools exe = new CommandTools("172.16.33.72", "cqaupdate", "cqaupdate");
		String ret = exe.readScript("/sedata/python_tools/distribute_2_beijing","distribute_2_beijing.py");
		//System.out.println( exe.exec("head .bashrc") );
		//exe.exec("cat .bashrc");
		if (ret=="Authenticationfailure") {
			System.out.println("登陆失败！");
		}else if(ret == "error" ){
			System.out.println("命令执行失败");
		}else{
			System.out.println(ret);
		}
		
	}
}