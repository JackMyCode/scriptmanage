package com.iflytek.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.iflytek.dao.basedao.BaseDao;
import com.iflytek.model.Account;
import com.iflytek.model.Script;
import com.iflytek.model.Server;
import com.iflytek.tools.ExecuteTools;
import com.iflytek.util.Util;

public class ScriptDao implements BaseDao<Script> {

	Connection conn = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	
	public List<Script> selectAll() {
				List<Script> Scriptlist = new ArrayList<Script>() ;
				
				Script script = null;
				Util util = new Util();
				
				conn = util.getConncetion();
				//String sql = "select * from script  order by count desc limit "+5+","+10;
				String sql = "select * from script";
				try {
					ps = conn.prepareStatement(sql);
					rs = ps.executeQuery();
					while (rs.next()){
						script = new Script();
						
						script.setScriptId(rs.getLong("scriptId"));
						script.setIp(rs.getString("ip"));
						script.setDir(rs.getString("dir"));
						script.setScriptName(rs.getString("scriptName"));
						script.setType(rs.getInt("type"));
						script.setCanWork(rs.getInt("canWork"));
						
						AccountDao accountDao = new AccountDao();
						Account object = new Account();
						object.setUserId(rs.getInt("userId"));
						script.setUser(accountDao.selectPrecise(object));
						
						int serverid = rs.getInt("serverId");
						Server server = new Server();
						server.setServerId(serverid);
						script.setServer(new ServerDao().selectPrecise(server));
						
						/*ExecuteTools exe = new ExecuteTools(script.getIp(), script.getUser().getAccount(), script.getUser().getPasswd());
						//System.out.println(script.getIp()+"|"+ script.getUser().getAccount()+"|"+  script.getUser().getPasswd());
						String content = null;
						try {
							content = exe.catScript(script.getDir(),script.getScriptName(),script.getType());
							//System.out.println(script.getDir()+"|"+script.getScriptName()+"|"+script.getType());
							//Thread.sleep(5000);
							//System.out.println(content);
							
							script.setContent(content);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}*/
						
						Scriptlist.add(script);
					}
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return Scriptlist;
	}

	public List<Script> select(Script object) {
		// TODO Auto-generated method stub
		List<Script> Scriptlist = new ArrayList<Script>() ;
		
		Script script = null;
				
		Util util = new Util();
		
		conn = util.getConncetion();
		String sql = "select * from script where scriptId like '%"+object.getScriptId()+"%'";
		try {
			ps = conn.prepareStatement(sql);
		//	rs = ps.executeQuery();
			rs = ps.executeQuery();
			while (rs.next()){
				script = new Script();
				
				script.setScriptId(rs.getLong("scriptId"));
				script.setIp(rs.getString("ip"));
				script.setDir(rs.getString("dir"));
				script.setScriptName(rs.getString("scriptName"));
				script.setType(rs.getInt("type"));
				script.setCanWork(rs.getInt("canWork"));
				
				AccountDao accountDao = new AccountDao();
				Account account = new Account();
				account.setUserId(rs.getInt("userId"));
				
				int serverid = rs.getInt("serverId");
				Server server = new Server();
				server.setServerId(serverid);
				script.setServer(new ServerDao().selectPrecise(server));
				
				script.setUser(accountDao.selectPrecise(account));
				
				Scriptlist.add(script);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Scriptlist;
	}
	
	public Script selectPrecise(Script object) {
		Script script = null;	
		Util util = new Util();
		conn = util.getConncetion();
		String sql = "select * from script where scriptId="+object.getScriptId();
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()){
				script = new Script();
				script.setScriptId(rs.getLong("scriptId"));
				script.setIp(rs.getString("ip"));
				script.setDir(rs.getString("dir"));
				script.setScriptName(rs.getString("scriptName"));
				script.setType(rs.getInt("type"));
				script.setCanWork(rs.getInt("canWork"));
				
				AccountDao accountDao = new AccountDao();
				Account account = new Account();
				account.setUserId(rs.getInt("userId"));
				
				int serverid = rs.getInt("serverId");
				Server server = new Server();
				server.setServerId(serverid);
				script.setServer(new ServerDao().selectPrecise(server));
				
				script.setUser(accountDao.selectPrecise(account));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return script;
	}

	public void delete(Script object) {
		// TODO Auto-generated method stub
		Util util = new Util();
		conn = util.getConncetion();
		
		String sql = "delete from script where scriptId=?";
		try {
			ps = conn.prepareStatement(sql);
		//	rs = ps.executeQuery();
			ps.setLong(1, object.getScriptId());
			ps.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void insert(Script object) {
		Util util = new Util();
		conn = util.getConncetion();
		
		String sql = "insert into script (ip,dir,scriptName,userId,canWork,type) values( ?,?,?,?,?,?)";
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, object.getIp());
			ps.setString(2, object.getDir());
			ps.setString(3, object.getScriptName());
			ps.setInt(4, object.getUser().getUserId());
			ps.setInt(5, object.getCanWork());
			ps.setInt(6, object.getType());
			ps.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void update(Script object) {
		// TODO Auto-generated method stub
		Util util = new Util();
		conn = util.getConncetion();
		
		String sql = "update script set ip=?,dir=?,scriptName=?,userId=?,canWork=?,type=? where scriptId=?";
		try {
			ps = conn.prepareStatement(sql);
		//	rs = ps.executeQuery();
			ps.setString(1, object.getIp());
			ps.setString(2, object.getDir());
			ps.setString(3, object.getScriptName());
			ps.setInt(4, object.getUser().getUserId());
			ps.setInt(5, object.getCanWork());
			ps.setInt(6, object.getType());
			ps.setLong(7, object.getScriptId());
			ps.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	public List<Script> select(String ip , String scriptName ) {
		
		List<Script> Scriptlist = new ArrayList<Script>() ;
		
		Script script = null;
		Util util = new Util();
		
		conn = util.getConncetion();
		//String sql = "select * from script  order by count desc limit "+5+","+10;
		String sql = "select * from script ";
		if( (!ip.equals("") && ip != "" && ip!=null && ip!="0" && !ip.equals("0")) && (!ip.equals("") && ip != "" && ip !=null) ){
			sql = sql + " where ip like '%"+ip+"%' and scriptName like '%"+scriptName+"%'";
		}else if ( !ip.equals("") && ip != "" && ip!=null && ip!="0" && !ip.equals("0")){
			sql = sql +  "where ip like '%"+ip+"%'";
		}else if( !ip.equals("") && ip != "" && ip !=null ){
			sql = sql + "where scriptName like '%"+scriptName+"%'";
		}
		System.out.println(sql);
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()){
				
				script = new Script();
				
				script.setScriptId(rs.getLong("scriptId"));
				script.setIp(rs.getString("ip"));
				script.setDir(rs.getString("dir"));
				script.setScriptName(rs.getString("scriptName"));
				script.setType(rs.getInt("type"));
				script.setCanWork(rs.getInt("canWork"));
				
				AccountDao accountDao = new AccountDao();
				Account object = new Account();
				object.setUserId(rs.getInt("userId"));
				script.setUser(accountDao.selectPrecise(object));
				
				int serverid = rs.getInt("serverId");
				Server server = new Server();
				server.setServerId(serverid);
				script.setServer(new ServerDao().selectPrecise(server));
				
				/*ExecuteTools exe = new ExecuteTools(script.getIp(), script.getUser().getAccount(), script.getUser().getPasswd());
				//System.out.println(script.getIp()+"|"+ script.getUser().getAccount()+"|"+  script.getUser().getPasswd());
				String content = null;
				try {
					content = exe.catScript(script.getDir(),script.getScriptName(),script.getType());
					//System.out.println(script.getDir()+"|"+script.getScriptName()+"|"+script.getType());
					//Thread.sleep(5000);
					//System.out.println(content);
					
					script.setContent(content);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
				
				Scriptlist.add(script);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Scriptlist;
}
}
