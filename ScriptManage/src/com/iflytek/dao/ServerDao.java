package com.iflytek.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.iflytek.dao.basedao.BaseDao;
import com.iflytek.model.Server;
import com.iflytek.util.Util;

public class ServerDao implements BaseDao<Server> {

	Connection conn = null;
	PreparedStatement ps = null;
	ResultSet rs = null;

	public List<Server> selectAll() {
				List<Server> Serverlist = new ArrayList<Server>() ;
				
				Server server = null;
						
				Util util = new Util();
				
				conn = util.getConncetion();
				String sql = "select * from server";
				try {
					ps = conn.prepareStatement(sql);
					rs = ps.executeQuery();
					while (rs.next()){
						server = new Server();
						server.setServerId(rs.getInt("serverId"));
						server.setServerName(rs.getString("serverName"));
						server.setIp(rs.getString("ip"));
						server.setArea(rs.getInt("area"));
						Serverlist.add(server);
					}
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return Serverlist;
	}

	public List<Server> select(Server object) {
		// TODO Auto-generated method stub
		List<Server> Serverlist = new ArrayList<Server>() ;
		
		Server server = null;
				
		Util util = new Util();
		
		conn = util.getConncetion();
		String sql = "select * from server where serverId like '%"+object.getServerId()+"%'";
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()){
				server = new Server();
				server.setServerId(rs.getInt("serverId"));
				server.setServerName(rs.getString("serverName"));
				server.setIp(rs.getString("ip"));
				Serverlist.add(server);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Serverlist;
	}
	
	public Server selectPrecise(Server object) {
		Server server = null;	
		Util util = new Util();
		conn = util.getConncetion();
		String sql = "select * from server where serverId="+object.getServerId();
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()){
				server = new Server();
				server.setServerId(rs.getInt("serverId"));
				server.setServerName(rs.getString("serverName"));
				server.setIp(rs.getString("ip"));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return server;
	}
	
	public List<Server> selectArea(int type) {
		List<Server> Serverlist = new ArrayList<Server>() ;
		
		Server server = null;
				
		Util util = new Util();
		
		conn = util.getConncetion();
		String sql = "select * from server";
		if( type != 4 && type != 0 ){
			sql = sql +" where area="+type;
		}
		
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()){
				server = new Server();
				server.setServerId(rs.getInt("serverId"));
				server.setServerName(rs.getString("serverName"));
				server.setIp(rs.getString("ip"));
				server.setArea(rs.getInt("area"));
				Serverlist.add(server);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Serverlist;
}
/*
	public void delete(Server object) {
		// TODO Auto-generated method stub
		Util util = new Util();
		conn = util.getConncetion();
		
		String sql = "delete from Server where userId=?";
		try {
			ps = conn.prepareStatement(sql);
		//	rs = ps.executeQuery();
			ps.setInt(1, object.getUserId());
			ps.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void insert(Server object) {
		Util util = new Util();
		conn = util.getConncetion();
		
		String sql = "insert into Server (Server, passwd) values( ?,?)";
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, object.getServer());
			ps.setString(2, object.getPasswd());
			ps.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void update(Server object) {
		// TODO Auto-generated method stub
		Util util = new Util();
		conn = util.getConncetion();
		
		String sql = "update Server set Server=?,passwd=? where userId=?";
		try {
			ps = conn.prepareStatement(sql);
		//	rs = ps.executeQuery();
			ps.setString(1, object.getServer());
			ps.setString(2, object.getPasswd());
			ps.setInt(3, object.getUserId());
			ps.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/

	
}
