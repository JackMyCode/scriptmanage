<!DOCTYPE html>
<%@page import="org.apache.taglibs.standard.tag.common.core.ForEachSupport"%>
<%@page import="com.iflytek.model.Script"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<html lang="en">
<head>        
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />    
    <!--[if gt IE 8]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />        
    <![endif]-->                
    <title>Script Management</title>
    <link rel="icon" type="image/ico" href="favicon.ico"/>
    
    <link href="/ScriptManage/css/stylesheets.css" rel="stylesheet" type="text/css" />
    <!--[if lte IE 7]>
        <link href="css/ie.css" rel="stylesheet" type="text/css" />
        <script type='text/javascript' src='/ScriptManage/js/plugins/other/lte-ie7.js'></script>
    <![endif]-->      
    <script type='text/javascript' src='/ScriptManage/js/plugins/jquery/jquery-1.9.1.min.js'></script>
    <script type='text/javascript' src='/ScriptManage/js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='/ScriptManage/js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='/ScriptManage/js/plugins/jquery/globalize.js'></script>
    <script type='text/javascript' src='/ScriptManage/js/plugins/other/excanvas.js'></script>
    
    <script type='text/javascript' src='/ScriptManage/js/plugins/other/jquery.mousewheel.min.js'></script>
        
    <script type='text/javascript' src='/ScriptManage/js/plugins/bootstrap/bootstrap.min.js'></script>            
    
    <script type='text/javascript' src='/ScriptManage/js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>    
    
    <script type='text/javascript' src="/ScriptManage/js/plugins/uniform/jquery.uniform.min.js"></script>
    
    <script type='text/javascript' src='/ScriptManage/js/plugins/datatables/jquery.dataTables.min.js'></script>
    
    <script type='text/javascript' src='/ScriptManage/js/plugins/shbrush/XRegExp.js'></script>
    <script type='text/javascript' src='/ScriptManage/js/plugins/shbrush/shCore.js'></script>
    <script type='text/javascript' src='/ScriptManage/js/plugins/shbrush/shBrushXml.js'></script>
    <script type='text/javascript' src='/ScriptManage/js/plugins/shbrush/shBrushJScript.js'></script>
    <script type='text/javascript' src='/ScriptManage/js/plugins/shbrush/shBrushCss.js'></script>    
    
    <script type='text/javascript' src='/ScriptManage/js/plugins.js'></script>
    <script type='text/javascript' src='/ScriptManage/js/charts.js'></script>
    <script type='text/javascript' src='/ScriptManage/js/actions.js'></script>
</head>
<body>    
    <div id="loader"><img src="/ScriptManage/img/loader.gif"/></div>
    <div class="wrapper">
        
        <div class="sidebar">
            
            
            
            <ul class="navigation">
                <li>
                    <a href="#" class="blyellow">在线脚本管理</a>
                    <div class="open"></div>
                    <ul>
                        <li><a href="/ScriptManage/loadScript.jsp">添加脚本</a></li>
                        <li><a href="/ScriptManage/servlet/ScriptServletList">查找脚本</a></li>
                    </ul>
                </li>
            </ul>           
        </div>

        <%--<div class="sidebar">
            
            
            
            <ul class="navigation">
                <li>
                    <a href="#" class="blyellow">脚本管理系统</a>
                    <div class="open"></div>
                    <ul>
                        <li><a href="ui.html">添加脚本</a></li>
                        <li><a href="grid_sys.html">Grid System</a></li>
                    </ul>
                </li>
            </ul>           
        </div>
        
        --%><div class="body">
            <div class="content">
            	 <div class="page-header">
                    <div class="icon">
                        <span class="ico-layout-7"></span>
                    </div>
                    <h1>Script Management<small>Add script</small></h1>
                </div>
                
               <div class="row-fluid">
				  <div class="span9">
					<form action="" method="get">
					
                            
							<div class="span2"><input type="file" class="file" id="fileField" onchange="document.getElementById('textfield').value=this.value" /></div>
							<div class="span5"><input type='text' name='textfield' id='textfield' class='txt' />  </div>
							<br>
                            <%--<div class="row-form">
                                    <div class="span3">File:</div>
                                    <div class="span9">                            
                                        <div class="input-append file">
                                            <input type="file" name="file"/>
                                            <input type="text"/>
                                            <button onchange="document.getElementById('textfield').value=this.value" >Browse</button>
                                        </div>                            
                                    </div>
                            </div>
                            
                            <div class="span2">
		                                        <select id="area">
		                                        <option value="0" selected="selected">请选择地区</option>
												  <option value="all">全部</option>
												  <option value="hf">安徽</option>
												  <option value="bj">北京</option>
												  <option value="gz">广州</option>
												</select>
		                                    </div>
		                                    
		                                    <div class="span1">请选择服务器：</div>
		                                    <div class="span2">
		                                        <select id="server" name="ip">
		                                        	<option value="0">请选择服务器</option>
		                                        </select>
		                                    </div>
                                
                            --%><div class="head">                                
                                <h2>目的文件信息</h2>                                                                   
                            </div>                  
                            <div class="data-fluid">
                                <div class="row-form">
                                    <div class="span3">选择地区:</div>
                                    
                                    <div class="span9">
                                        <select id="area">
		                                    <option value="0" selected="selected">请选择地区</option>
											<option value="all">全部</option>
											<option value="hf">安徽</option>
											<option value="bj">北京</option>
											<option value="gz">广州</option>
										</select>
                                    </div>
                                </div>                    

                                <div class="row-form">
                                    <div class="span3">请选择服务器：</div>
		                               <div class="span9">
			                               <select id="server" name="ip">
			                                	<option value="0">请选择服务器</option>
			                                </select>
		                               </div>
                                </div>                    

                                <div class="row-form">
                                    <div class="span3">脚本所在目录:</div>
                                    <div class="span9">
                                        <input type="text" class="tags" name="dir"/>
                                    </div>
                                </div>    
                                
                                
                                
                                <div class="row-form">
                                    <div class="span3">脚本名:</div>
                                    <div class="span9">
                                        <input type="text" class="tags" name="scriptName"/>
                                    </div>
                                </div>  
                                
                                <div class="row-form">
                                	<div class="span3">用户名：:</div>
                                	<div class="span9">
                                        <select name="account" onChange="myselect()">
										  <option>请选择用户：</option>
										  <option value="qb">zfwu</option>
										  <option value="hf">dmzhang</option>
										  <option value="bj">cqaupdate</option>
										</select>
                                	</div>
                                </div>
                                
                                <div class="row-form">
                                    <div class="span3">用户密码:</div>
                                    <div class="span9">
                                        <input type="password" class="tags" name="scriptName"/>
                                    </div>
                                </div>  
                                
                                                             

                                <div class="row-form">
                                    <div class="span3">脚本类型:</div>
                                    <div class="span9">
                                        <input type="radio" checked="checked" name="type" value="1"/>python脚本 
                                        <input type="radio" name="type" value="2"/>shell脚本
                                        <input type="radio" name="type" value="3"/>其他 
                                    </div>
                                </div>

                                <div class="row-form">
                                    <div class="span3">是否可运行:</div>
                                    <div class="span9">
                                        <input type="radio" checked="checked" name="canWork" value="1"/>是 
                                        <input type="radio" name="canWork" value="0"/>否
                                    </div>
                                </div>   
                            </div>
                        </div>

                    </div>
                   <br>
                    <div class="span11" align="center">
                   	 	<button class="btn" type="reset" value="">取消</button>  
                    	<button class="btn" type="submit" value="">上传</button>  
                    </div>
                  </form>
                    
                </div>
            
            </div></div>
        </div>  
    </div>
    <div class="dialog" id="source" style="display: none;" title="Source"></div>   

</body>

<script type="text/javascript">
	$(function() {
		$("#area").change(function() {
			$("#server").find("option").remove();
			$("#server").append("<option value='0'>请选择服务器</option>");
			$('option:selected', this).each(function(){ //印出選到多個值 
				area = this.value;
			        $.post("/ScriptManage/servlet/GetServerInf",{area:area},function(data) {
			        	$.each(data,function(entryIndex,entry) {
			        		$("#server").append("<option value="+entry['ip']+">"+entry['ip']+"</option>");
			        	});
			        },"JSON");
				}); 
		});
	});
</script>   
</html>
