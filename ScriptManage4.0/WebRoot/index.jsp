<!DOCTYPE html>
<%@page import="org.apache.taglibs.standard.tag.common.core.ForEachSupport"%>
<%@page import="com.iflytek.model.Script"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<%@ page contentType="text/html; charset=utf-8"%>
<html lang="en">
<head>        
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />    
    <!--[if gt IE 8]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />        
    <![endif]-->                
    <title>Script Management</title>
    <link rel="icon" type="image/ico" href="favicon.ico"/>
    
    <link href="/ScriptManage/css/stylesheets.css" rel="stylesheet" type="text/css" />
    <!--[if lte IE 7]>
        <link href="css/ie.css" rel="stylesheet" type="text/css" />
        <script type='text/javascript' src='/ScriptManage/js/plugins/other/lte-ie7.js'></script>
    <![endif]-->      
    <script type='text/javascript' src='/ScriptManage/js/plugins/jquery/jquery-1.9.1.min.js'></script>
    <script type='text/javascript' src='/ScriptManage/js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='/ScriptManage/js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='/ScriptManage/js/plugins/jquery/globalize.js'></script>
    <script type='text/javascript' src='/ScriptManage/js/plugins/other/excanvas.js'></script>
    
    <script type='text/javascript' src='/ScriptManage/js/plugins/other/jquery.mousewheel.min.js'></script>
        
    <script type='text/javascript' src='/ScriptManage/js/plugins/bootstrap/bootstrap.min.js'></script>            
    
    <script type='text/javascript' src='/ScriptManage/js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>    
    
    <script type='text/javascript' src="/ScriptManage/js/plugins/uniform/jquery.uniform.min.js"></script>
    
    <script type='text/javascript' src='/ScriptManage/js/plugins/datatables/jquery.dataTables.min.js'></script>
    
    <script type='text/javascript' src='/ScriptManage/js/plugins/shbrush/XRegExp.js'></script>
    <script type='text/javascript' src='/ScriptManage/js/plugins/shbrush/shCore.js'></script>
    <script type='text/javascript' src='/ScriptManage/js/plugins/shbrush/shBrushXml.js'></script>
    <script type='text/javascript' src='/ScriptManage/js/plugins/shbrush/shBrushJScript.js'></script>
    <script type='text/javascript' src='/ScriptManage/js/plugins/shbrush/shBrushCss.js'></script>    
    
    <script type='text/javascript' src='/ScriptManage/js/plugins.js'></script>
    <script type='text/javascript' src='/ScriptManage/js/charts.js'></script>
    <script type='text/javascript' src='/ScriptManage/js/actions.js'></script>
</head>
<body>    
    <div id="loader"><img src="/ScriptManage/img/loader.gif"/></div>
    <div class="wrapper">
        
        <div class="sidebar">
            
            
            
            <ul class="navigation">
                <li>
                    <a href="#" class="blyellow">在线脚本管理</a>
                    <div class="open"></div>
                    <ul>
                        <li><a href="/ScriptManage/AddScript.jsp">添加脚本</a></li>
                        <li><a href="/ScriptManage/servlet/ScriptServletList">查找脚本</a></li>
                    </ul>
                </li>
            </ul>           
        </div>
        
        <div class="body">
            <div class="content">
                
                <div class="page-header">
                    <div class="icon">
                        <span class="ico-layout-7"></span>
                    </div>
                    <h1>Script Management<small>Manage server script</small></h1>
                </div>

                <div class="row-fluid">
                    <div class="span12">            

                        <div class="block">
                            <div class="head blue">
                                <div class="icon"><span class="ico-layout-9"></span></div>
                                <h2>Script Information</h2>                            
                            </div>  
                            	<div class="data-fluid">
	                                <div class="row-form">
	                                	<form action="/ScriptManage/servlet/SearchScript" method="get">
		                                    <div class="span1">选择地区:</div>    
		                                    <div class="span2">
		                                        <select id="area">
		                                        <option value="0" selected="selected">请选择地区</option>
												  <option value="all">全部</option>
												  <option value="hf">安徽</option>
												  <option value="bj">北京</option>
												  <option value="gz">广州</option>
												</select>
		                                    </div>
		                                    
		                                    <div class="span1">请选择服务器：</div>
		                                    <div class="span2">
		                                        <select id="server" name="ip">
		                                        	<option value="0">请选择服务器</option>
		                                        </select>
		                                    </div>
		                                    
		                                    <div class="span1">脚本名：</div>
		                                    <div class="span2">
		                                        <input type="text" name="scriptName"/>
		                                    </div>
	                                    	<div class="span3">
	                                    		<button type="submit"><div class="icon"><span class="icon-search"></span></div></button>
	                                    	</div>
	                                    </form>
	                              </div>
	                          </div>                  
                                              
                                <div class="data-fluid">
                                    <%-- <table class="table fTable lcnp" cellpadding="0" cellspacing="0" width="100%">
                                    --%>
                                        <table class="table" cellpadding="0" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                            	<!-- <th><input type="checkbox" class="checkall"/></th>
                                            	<th>Id</th> -->
                                                <th width="20%">IP</th>
                                                <th>Directory</th>
                                                <th width="20%">ScriptName</th>
                                                <th width="20%">Ower</th>
                                                <th width="120" class="TAC">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        	<c:forEach items="${list}" var="object" >
                                            <tr>
                                            	<!-- <td><input type="checkbox" name="order[]" value="528"/></td> -->
                                            	<td hidden="true" id="Id">${object.scriptId }</td>
                                                <td value="${object.server.ip }">${object.server.ip }</td>
                                                <td>${object.dir }</td>
                                                <td><span>${object.scriptName }</span></td>
                                                <td>${object.user.account }</td>
                                                <td>

													<a href="#" class="button blue" data-toggle="modal" data-target="#Look${object.scriptId }"><div class="icon"><span class=" ico-eye"></span></div></a>
													<div class="modal fade" id="Look${object.scriptId }" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
													  <div class="modal-dialog">
													    <div class="modal-content">
													      <div class="modal-header"><h4 class="modal-title" id="myModalLabel">${object.server.ip }${object.dir }/${object.scriptName }</h4></div>
													      <div class="modal-body"><pre><pre></pre></pre></div>
													      <div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>
													    </div>
													  </div>
													</div>
													
													<a href="#" class="button green" data-toggle="modal" data-target="#Edit${object.scriptId }"><div class="icon"><span class=" ico-pencil"></span></div></a>
                                                	<a href="#" class="button red"><div class="icon"><span class="ico-signin"></span></div></a> 
                                                	                         
                                                </td>
                                            </tr> 
                                            </c:forEach>                              
                                        </tbody>
                                    </table>                    
                                </div> 
                        </div>         
                    </div>
                </div>  
            </div>
        </div>  
    </div>
    <div class="dialog" id="source" style="display: none;" title="Source"></div>      
</body>
<script type="text/javascript">

	$(function() {
		$("#area").change(function() {
			$("#server").find("option").remove();
			$("#server").append("<option value='0'>请选择服务器</option>");
			$('option:selected', this).each(function(){ //印出選到多個值 
				area = this.value;
			        $.post("/ScriptManage/servlet/GetServerInf",{area:area},function(data) {
			        	$.each(data,function(entryIndex,entry) {
			        		$("#server").append("<option value="+entry['serverId']+">"+entry['ip']+"</option>");
			        	});
			        },"JSON");
				}); 
		});
	});

	$(function() {
		$(".button.blue").each(function(){
			$(this).click(function(){
				scriptId = $(this).parent().parent().children("td").eq(0).text();
				
				$.post("/ScriptManage/servlet/CheckScript",{scriptId:scriptId},function(data) {
					$(".modal-body").children("pre").html(data);
		        	});
				}); 
		});
	});
	
	$(function() {
		$(".button.green").each(function(){
			$(this).click(function(){
					$.get("/servlet/CheckScript",{scriptId:$(this).parent().parent().children("td").eq(0).text() },function(data) {
					})
				})
		})
	});
	
	$(function() {
		$(".button.red").each(function(){
			$(this).click(function(){
					//alert($(this).parent().parent().children("td").eq(1).text())
					$.get("/ScriptManage/servlet/ExecuteScript",{scriptId:$(this).parent().parent().children("td").eq(1).text() },function(data) {
					})
				})
		})
	});
	
</script>
</html>
