<!DOCTYPE html>
<%@page import="org.apache.taglibs.standard.tag.common.core.ForEachSupport"%>
<%@page import="com.iflytek.model.Script"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<%@ page contentType="text/html; charset=utf-8"%>
<html lang="en">
<head>        
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />    
    <!--[if gt IE 8]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />        
    <![endif]-->                
    <title>Script Management</title>
    <link rel="icon" type="image/ico" href="favicon.ico"/>
    
    <link href="/ScriptManage/css/stylesheets.css" rel="stylesheet" type="text/css" />
    <!--[if lte IE 7]>
        <link href="css/ie.css" rel="stylesheet" type="text/css" />
        <script type='text/javascript' src='/ScriptManage/js/plugins/other/lte-ie7.js'></script>
    <![endif]-->      
    <script type='text/javascript' src='/ScriptManage/js/plugins/jquery/jquery-1.9.1.min.js'></script>
    <script type='text/javascript' src='/ScriptManage/js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
    <script type='text/javascript' src='/ScriptManage/js/plugins/jquery/jquery-migrate-1.1.1.min.js'></script>
    <script type='text/javascript' src='/ScriptManage/js/plugins/jquery/globalize.js'></script>
    <script type='text/javascript' src='/ScriptManage/js/plugins/other/excanvas.js'></script>
    
    <script type='text/javascript' src='/ScriptManage/js/plugins/other/jquery.mousewheel.min.js'></script>
        
    <script type='text/javascript' src='/ScriptManage/js/plugins/bootstrap/bootstrap.min.js'></script>            
    
    <script type='text/javascript' src='/ScriptManage/js/plugins/cookies/jquery.cookies.2.2.0.min.js'></script>    
    
    <script type='text/javascript' src="/ScriptManage/js/plugins/uniform/jquery.uniform.min.js"></script>
    
    <script type='text/javascript' src='/ScriptManage/js/plugins/datatables/jquery.dataTables.min.js'></script>
    
    <script type='text/javascript' src='/ScriptManage/js/plugins/shbrush/XRegExp.js'></script>
    <script type='text/javascript' src='/ScriptManage/js/plugins/shbrush/shCore.js'></script>
    <script type='text/javascript' src='/ScriptManage/js/plugins/shbrush/shBrushXml.js'></script>
    <script type='text/javascript' src='/ScriptManage/js/plugins/shbrush/shBrushJScript.js'></script>
    <script type='text/javascript' src='/ScriptManage/js/plugins/shbrush/shBrushCss.js'></script>    
    
    <script type='text/javascript' src='/ScriptManage/js/plugins.js'></script>
    <script type='text/javascript' src='/ScriptManage/js/charts.js'></script>
    <script type='text/javascript' src='/ScriptManage/js/actions.js'></script>
</head>
<body>    
    <div id="loader"><img src="/ScriptManage/img/loader.gif"/></div>
    <div class="wrapper">
        
        <div class="sidebar">
            
            
            
            <ul class="navigation">
                <li>
                    <a href="#" class="blyellow">在线脚本管理</a>
                    <ul>
                        <li><a href="/ScriptManage/AddScript.jsp">添加脚本</a></li>
                        <li><a href="/ScriptManage/servlet/ScriptServletList">查找脚本</a></li>
                    </ul>
                </li>
            </ul>           
        </div>
        
        <div class="body">
            <div class="content">
                
                <div class="page-header">
                    <div class="icon">
                        <span class="ico-layout-7"></span>
                    </div>
                    <h1>Script Management<small>Manage server script</small></h1>
                </div>

                <div class="row-fluid">
                    <div class="span12">            

                        <div class="block">
                            <div class="head blue">
                                <div class="icon"><span class="ico-layout-9"></span></div>
                                <h2>Load Script </h2>                            
                            </div>  
                            	<div class="data-fluid">
	                                <div class="row-form">
	                                	<form action="/ScriptManage/servlet/LoadScript" method="get" enctype="multipart/form-data">
	                                		<%-- select local script --%>
	                                		<div class="span12">
	                                			<div class="span4">
	                                				
											        <input type="file" name="upfile"/><%--
											       	<input type="submit" id="subm" value="submit"/>
	                                			--%></div>
											</div>
											<br>
											<%-- select server  --%>
											<div class="span12">
			                                    <div class="span2">选择上传地区:</div>    
			                                    <div class="span2">
			                                        <select id="area">
			                                        <option value="0" selected="selected">请选择地区</option>
													  <option value="all">全部</option>
													  <option value="hf">安徽</option>
													  <option value="bj">北京</option>
													  <option value="gz">广州</option>
													</select>
			                                    </div>
			                                </div>
			                                <br>
			                                
			                                <div class="span12">    
			                                    <div class="span2" selected="selected">请选择上传服务器：</div>
			                                    <div class="span2">
			                                        <select id="server" name="ip">
			                                        	<option value="0">请选择服务器</option>
			                                        </select>
			                                    </div>
			                               </div>
			                               <br>
			                               
			                               <div class="span12"> 
			                                    <div class="span2" selected="selected">请选择用户</div>
			                                    <div class="span2">
			                                        <select id="account" name="account">
			                                        	<option value="0">请选择用户</option>
			                                        </select>
			                                    </div>
		                                    </div>
		                                    <br>
		                                    
		                                    <div class="span12">
			                                    <div class="span2">脚本上传目录:</div>
			                                    <div class="span4">
			                                        <input type="text" name="dir"/>
			                                    </div>
			                                </div> 
			                                <br>
			                                
			                                <div class="span6" align="center">
						                   	 	<button class="btn" type="reset" value="">取消</button>  
						                    	<button class="btn" type="submit" value="">上传</button>  
						                    </div>   
	                                    </form>
	                              	</div>
	                          	</div>                  
                        </div>         
                    </div>
                </div>  
            </div>
        </div>  
    </div>
    <div class="dialog" id="source" style="display: none;" title="Source"></div>      
</body>
<script type="text/javascript">
	$(function() {
		$("#subm").click(function() {
			alert("this ok!");
			$.post("/ScriptManage/servlet/LoadScript",function(data) {
			   },"JSON");
		});
	});
	
	$(function() {
		$("#area").change(function() {
			$("#server").find("option").remove();
			$("#server").append("<option value='0'>请选择服务器</option>");
			$('option:selected', this).each(function(){ //印出選到多個值 
				area = this.value;
			        $.post("/ScriptManage/servlet/GetServerInf",{area:area},function(data) {
			        	$.each(data,function(entryIndex,entry) {
			        		$("#server").append("<option value="+entry['serverId']+">"+entry['ip']+"</option>");
			        	});
			        },"JSON");
				}); 
		});
	});
	
	$(function() {
		$("#server").change(function() {
			serverId = this.value ;
			$("#account").find("option").remove();
			$("#account").append("<option value='0'>请选择用户</option>");
			$('option:selected', this).each(function(){ //印出選到多個值 
			        $.post("/ScriptManage/servlet/GetAccountInf",{serverId:serverId},function(data) {
			        	$.each(data,function(entryIndex,entry) {
			        		$("#account").append("<option value="+entry['userId']+">"+entry['account']+"</option>");
			        	});
			        },"JSON");
				}); 
		});
	});
	
	function inputFileOnChange() {    
	    if(document.getElementById('my-file').files) {
	        // Support: nsIDOMFile, nsIDOMFileList
	        //alert('value: ' + document.getElementById('my-file').value);
	        //alert('files.length: ' + document.getElementById('my-file').files.length);
	        //alert('fileName: ' + document.getElementById('my-file').files.item(0).fileName);
	       // alert('fileSize: ' + document.getElementById('my-file').files.item(0).fileSize);
	       // alert('dataurl: ' + document.getElementById('my-file').files.item(0).getAsDataURL());
	       // alert('data: ' + document.getElementById('my-file').files.item(0).getAsBinary());
	       // alert('datatext: ' + document.getElementById('my-file').files.item(0).getAsText("utf-8"));
	    };
	};
	
</script>
</html>
