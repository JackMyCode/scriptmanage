package com.iflytek.model;

public class Account {
	
	private int userId;
	private String account;
	private String passwd;
	private Server server;
	
	public Account(int userId, String account, String passwd, Server server) {
		super();
		this.userId = userId;
		this.account = account;
		this.passwd = passwd;
		this.server = server;
	}
	public Server getServer() {
		return server;
	}
	public Account() {
		super();
	}
	public void setServer(Server server) {
		this.server = server;
	}
	public Account(int userId) {
		super();
		this.userId = userId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	
	

}
