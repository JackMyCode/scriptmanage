package com.iflytek.model;

import java.util.List;

public class RoleMenu {

	private Roles roles;
	private List<MenuTree> menu;
	
	public Roles getRoles() {
		return roles;
	}
	public void setRoles(Roles roles) {
		this.roles = roles;
	}
	public List<MenuTree> getMenu() {
		return menu;
	}
	public void setMenu(List<MenuTree> menu) {
		this.menu = menu;
	}
	
}
