package com.iflytek.model;

public class MenuTree {

	private int treeId;
	private String title;
	private String desc;
	private MenuTree parentMenu;
	private String url;
	
	public int getTreeId() {
		return treeId;
	}
	public void setTreeId(int treeId) {
		this.treeId = treeId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public MenuTree getParentMenu() {
		return parentMenu;
	}
	public void setParentMenu(MenuTree parentMenu) {
		this.parentMenu = parentMenu;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	
}
