package com.iflytek.model;

public class Roles {
	private int roleId;
	private String roleName;
	private String roleDesc;
	private String perMission;
	
	public int getRoleId() {
		return roleId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getRoleDesc() {
		return roleDesc;
	}
	public void setRoleDesc(String roleDesc) {
		this.roleDesc = roleDesc;
	}
	public String getPerMission() {
		return perMission;
	}
	public void setPerMission(String perMission) {
		this.perMission = perMission;
	}
	
	
}
