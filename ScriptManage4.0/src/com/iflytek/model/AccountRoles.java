package com.iflytek.model;

import java.util.List;


public class AccountRoles {

	private Account account;
	private List<Roles> roles;
	
	public Account getAccount() {
		return account;
	}
	
	public void setAccount(Account account) {
		this.account = account;
	}
	
	public List<Roles> getRoles() {
		return roles;
	}
	
	public void setRoles(List<Roles> roles) {
		this.roles = roles;
	}
}
