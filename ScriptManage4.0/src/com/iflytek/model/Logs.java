package com.iflytek.model;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Logs {
	
	private long logId;
	
	private Script script;
	private String patchContent;
	private String whenChanged;
	
	public long getLogId() {
		return logId;
	}
	public Script getScript() {
		return script;
	}
	public void setScript(Script script) {
		this.script = script;
	}
	public void setLogId(long logId) {
		this.logId = logId;
	}
	
	public String getPatchContent() {
		return patchContent;
	}
	public void setPatchContent(String patchContent) {
		this.patchContent = patchContent;
	}
	public String getWhenChanged() {
		return whenChanged;
	}
	public void setWhenChanged(String whenChanged) {
		this.whenChanged = whenChanged;
	}
	
	public void setNowtoWhenChanged() {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		this.whenChanged = df.format(new Date());
	}
	
	public static void main(String args[]){
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//设置日期格式
		System.out.println(df.format(new Date()));// new Date()为获取当前系统时间
	}
}
