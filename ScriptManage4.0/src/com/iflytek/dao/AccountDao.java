package com.iflytek.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.iflytek.dao.basedao.BaseDao;
import com.iflytek.model.Account;
import com.iflytek.model.Server;
import com.iflytek.util.Util;

public class AccountDao implements BaseDao<Account> {

	Connection conn = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	
	public Account select(String userName){
		Account account = null;	
		Util util = new Util();
		conn = util.getConncetion();
		String sql = "select * from account where account="+userName;
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()){
				account = new Account();
				account.setUserId(rs.getInt("userId"));
				account.setAccount(rs.getString("account"));
				account.setPasswd(rs.getString("passwd"));
				int serverid = rs.getInt("serverId");
				Server server = new Server();
				server.setServerId(serverid);
				account.setServer(new ServerDao().selectPrecise(server));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			util.closeReslutSet(rs);
			util.closePreparedStetement(ps);
			util.closeConnection(conn);
		}
		return account;
	}

	public List<Account> selectAll() {
				List<Account> accountlist = new ArrayList<Account>() ;
				
				Account account = null;
						
				Util util = new Util();
				
				conn = util.getConncetion();
				String sql = "select * from account";
				try {
					ps = conn.prepareStatement(sql);
					rs = ps.executeQuery();
					while (rs.next()){
						account = new Account();
						account.setUserId(rs.getInt("userId"));
						account.setAccount(rs.getString("account"));
						account.setPasswd(rs.getString("passwd"));
						
						int serverid = rs.getInt("serverId");
						Server object = new Server();
						object.setServerId(serverid);
						account.setServer(new ServerDao().selectPrecise(object));
						
						accountlist.add(account);
					}
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return accountlist;
	}

	public List<Account> select(Account object) {
		// TODO Auto-generated method stub
		List<Account> accountlist = new ArrayList<Account>() ;
		
		Account account = null;
				
		Util util = new Util();
		
		conn = util.getConncetion();
		String sql = "select * from account where userId like '%"+object.getUserId()+"%'";
		try {
			ps = conn.prepareStatement(sql);
		//	rs = ps.executeQuery();
			rs = ps.executeQuery();
			while (rs.next()){
				account = new Account();
				account.setUserId(rs.getInt("userId"));
				account.setAccount(rs.getString("account"));
				account.setPasswd(rs.getString("passwd"));
				
				int serverid = rs.getInt("serverId");
				Server server = new Server();
				server.setServerId(serverid);
				account.setServer(new ServerDao().selectPrecise(server));
				
				accountlist.add(account);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			util.closeReslutSet(rs);
			util.closePreparedStetement(ps);
			util.closeConnection(conn);
		}
		
		return accountlist;
	}
	
	public Account selectPrecise(Account object) {
		Account account = null;	
		Util util = new Util();
		conn = util.getConncetion();
		String sql = "select * from account where userId="+object.getUserId();
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()){
				account = new Account();
				account.setUserId(rs.getInt("userId"));
				account.setAccount(rs.getString("account"));
				account.setPasswd(rs.getString("passwd"));
				int serverid = rs.getInt("serverId");
				Server server = new Server();
				server.setServerId(serverid);
				account.setServer(new ServerDao().selectPrecise(server));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			util.closeReslutSet(rs);
			util.closePreparedStetement(ps);
			util.closeConnection(conn);
		}
		return account;
	}

	public List<Account> selectAccount(int type) {
		List<Account> Serverlist = new ArrayList<Account>() ;
		
		Account account = null;
				
		Util util = new Util();
		
		conn = util.getConncetion();
		String sql = "select * from account";
		if( type != 0 ){
			sql = sql +" where serverId="+type;
		}
		
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()){
				account = new Account();
				account.setUserId(rs.getInt("userId"));
				account.setAccount(rs.getString("account"));
				account.setPasswd(rs.getString("passwd"));
				Serverlist.add(account);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			util.closeReslutSet(rs);
			util.closePreparedStetement(ps);
			util.closeConnection(conn);
		}
		return Serverlist;
	}
	
	public void delete(Account object) {
		// TODO Auto-generated method stub
		Util util = new Util();
		conn = util.getConncetion();
		
		String sql = "delete from account where userId=?";
		try {
			ps = conn.prepareStatement(sql);
		//	rs = ps.executeQuery();
			ps.setInt(1, object.getUserId());
			ps.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			util.closeReslutSet(rs);
			util.closePreparedStetement(ps);
			util.closeConnection(conn);
		}
	}

	public void insert(Account object) {
		Util util = new Util();
		conn = util.getConncetion();
		
		String sql = "insert into account (account, passwd) values( ?,?)";
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, object.getAccount());
			ps.setString(2, object.getPasswd());
			ps.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			util.closeReslutSet(rs);
			util.closePreparedStetement(ps);
			util.closeConnection(conn);
		}
	}

	public void update(Account object) {
		// TODO Auto-generated method stub
		Util util = new Util();
		conn = util.getConncetion();
		
		String sql = "update account set account=?,passwd=? where userId=?";
		try {
			ps = conn.prepareStatement(sql);
		//	rs = ps.executeQuery();
			ps.setString(1, object.getAccount());
			ps.setString(2, object.getPasswd());
			ps.setInt(3, object.getUserId());
			ps.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			util.closeReslutSet(rs);
			util.closePreparedStetement(ps);
			util.closeConnection(conn);
		}
	}

	
}
