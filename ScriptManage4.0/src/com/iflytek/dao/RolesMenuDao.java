package com.iflytek.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.iflytek.model.MenuTree;
import com.iflytek.model.Roles;
import com.iflytek.util.Util;

public class RolesMenuDao {

	Connection conn = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	
	public List<MenuTree> select(Roles role) {
		List<MenuTree> menulist = new ArrayList<MenuTree>() ;
		
		MenuTree menu = null;
				
		Util util = new Util();
		
		conn = util.getConncetion();
		String sql = "select * from roleMenu where roleId="+role.getRoleId();
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			rs.next();
			String menuString = rs.getString("menuTree");
			
			if( menuString==null || menuString.equals(""))
				return null;
			String[] menuStr = menuString.split(",");
			System.out.println("|"+menuString+"|");
			
			for (int i = 0 ; i<menuStr.length ;i++ ) {
				menu =new MenuTreeDao().selectPrecise(Integer.parseInt(menuStr[i]));
				menulist.add(menu);
			}
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			return null;
		}finally{
			util.closeReslutSet(rs);
			util.closePreparedStetement(ps);
			util.closeConnection(conn);
		}
		return menulist;
	}
	
	public static void main(String []args){
		Roles role = new Roles();
		role.setRoleId(1);
		List<MenuTree> menulist = new RolesMenuDao().select(role);
		for(int i = 0 ; i< menulist.size() ; i++){
			
			MenuTree menu = menulist.get(i);
			if(menu.getParentMenu()==null){
				System.out.println(menu.getTitle());
			}else{
				System.out.println(menu.getParentMenu().getTitle()+":"+menu.getTitle());
			}
		}
	}
}
