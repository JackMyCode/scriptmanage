package com.iflytek.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.iflytek.dao.basedao.BaseDao;
import com.iflytek.model.Logs;
import com.iflytek.model.Script;
import com.iflytek.util.Util;

public class LogsDao implements BaseDao<Logs> {

	Connection conn = null;
	PreparedStatement ps = null;
	ResultSet rs = null;

	public static void main(String[] args) {
		
	}
	
	public List<Logs> selectAll() {
				List<Logs> Logslist = new ArrayList<Logs>() ;
				Logs logs = null;
				Util util = new Util();
				
				conn = util.getConncetion();
				String sql = "select * from logs";
				try {
					ps = conn.prepareStatement(sql);
					rs = ps.executeQuery();
					while (rs.next()){
						logs = new Logs();
						logs.setLogId(rs.getInt("logId"));
						logs.setPatchContent(rs.getString("patchContent"));
						
						Script object = new Script();
						object.setScriptId(rs.getLong("scriptId"));
						
						ScriptDao scriptdao = new ScriptDao();
						Script script = scriptdao.selectPrecise(object);
						logs.setScript(script);
						logs.setNowtoWhenChanged();
						Logslist.add(logs);
					}
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}finally{
					util.closeReslutSet(rs);
					util.closePreparedStetement(ps);
					util.closeConnection(conn);
				}
				return Logslist;
	}

	public List<Logs> select(Logs object) {
		// TODO Auto-generated method stub
		List<Logs> Logslist = new ArrayList<Logs>() ;
		
		Logs logs = null;
				
		Util util = new Util();
		
		conn = util.getConncetion();
		String sql = "select * from logs where logId like '%"+object.getLogId()+"%'";
		try {
			ps = conn.prepareStatement(sql);
		//	rs = ps.executeQuery();
			rs = ps.executeQuery();
			while (rs.next()){
				logs = new Logs();
				logs.setLogId(rs.getInt("logId"));
				logs.setPatchContent(rs.getString("patchContent"));
				
				Script getScript = new Script();
				getScript.setScriptId(rs.getLong("scriptId"));
				
				ScriptDao scriptdao = new ScriptDao();
				Script script = scriptdao.selectPrecise(getScript);
				logs.setScript(script);
				
				logs.setNowtoWhenChanged();
				Logslist.add(logs);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			util.closeReslutSet(rs);
			util.closePreparedStetement(ps);
			util.closeConnection(conn);
		}
		return Logslist;
	}
	
	public Logs selectPrecise(Logs object) {
		Logs logs = null;	
		Util util = new Util();
		conn = util.getConncetion();
		String sql = "select * from logs where logId="+object.getLogId();
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()){
				logs = new Logs();
				logs.setLogId(rs.getInt("logId"));
				logs.setPatchContent(rs.getString("patchContent"));
				
				Script getScript = new Script();
				getScript.setScriptId(rs.getLong("scriptId"));
				
				ScriptDao scriptdao = new ScriptDao();
				Script script = scriptdao.selectPrecise(getScript);
				logs.setScript(script);
				
				logs.setNowtoWhenChanged();
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			util.closeReslutSet(rs);
			util.closePreparedStetement(ps);
			util.closeConnection(conn);
		}
		return logs;
	}

	public void delete(Logs object) {
		// TODO Auto-generated method stub
		Util util = new Util();
		conn = util.getConncetion();
		
		String sql = "delete from logs where logId=?";
		try {
			ps = conn.prepareStatement(sql);
		//	rs = ps.executeQuery();
			ps.setLong(1, object.getLogId());
			ps.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			util.closeReslutSet(rs);
			util.closePreparedStetement(ps);
			util.closeConnection(conn);
		}
	}

	public void insert(Logs object) {
		Util util = new Util();
		conn = util.getConncetion();
		
		String sql = "insert into logs (scriptId,patchContent,whenChanged) values( ?,?,?)";
		try {
			ps = conn.prepareStatement(sql);
			ps.setLong(1, object.getScript().getScriptId());
			ps.setString(2, object.getPatchContent());
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			ps.setString(3, df.format(new Date()) );
			ps.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			util.closeReslutSet(rs);
			util.closePreparedStetement(ps);
			util.closeConnection(conn);
		}
	}

	public void update(Logs object) {
		// TODO Auto-generated method stub
		Util util = new Util();
		conn = util.getConncetion();
		
		String sql = "update logs set scriptId=?,patchContent=?,whenChanged=?  where logId=?";
		try {
			ps = conn.prepareStatement(sql);
		//	rs = ps.executeQuery();
			ps.setLong(1, object.getScript().getScriptId());
			ps.setString(2, object.getPatchContent());
			
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			ps.setString(3, df.format(new Date()) );
			ps.setLong(4, object.getLogId());
			ps.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			util.closeReslutSet(rs);
			util.closePreparedStetement(ps);
			util.closeConnection(conn);
		}
	}

	
}
