package com.iflytek.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.iflytek.model.Roles;
import com.iflytek.util.Util;

public class RolesDao {

	Connection conn = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	
	public Roles selectPrecise(int roleId) {
		Roles role = null;	
		Util util = new Util();
		conn = util.getConncetion();
		String sql = "select * from roles where RoleId="+roleId;
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			rs.next();
			role = new Roles();
			role.setRoleId(rs.getInt("roleId"));
			role.setRoleName(rs.getString("roleName"));
			role.setRoleDesc(rs.getString("roleDesc"));

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			util.closeReslutSet(rs);
			util.closePreparedStetement(ps);
			util.closeConnection(conn);
		}
		return role;
	}
	
	
}
 
