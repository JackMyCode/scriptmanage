package com.iflytek.dao.basedao;

import java.util.List;

public interface BaseDao<T> {
	

	public List<T> selectAll();
	
	public List<T> select(T object);
	
	public T selectPrecise(T object);
/*	
	public void delete(T object);
	
	public void insert(T object);
	
	public void update(T object);*/
	

}
