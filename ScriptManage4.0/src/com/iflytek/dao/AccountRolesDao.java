package com.iflytek.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.iflytek.model.Account;
import com.iflytek.model.MenuTree;
import com.iflytek.model.Roles;
import com.iflytek.util.Util;

public class AccountRolesDao{

	Connection conn = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	
	public List<Roles> select(Account account) {
		List<Roles> Roleslist = new ArrayList<Roles>() ;
		
		Roles role = null;
				
		Util util = new Util();
		
		conn = util.getConncetion();
		String sql = "select * from accountRoles where accountId="+account.getUserId();
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			rs.next();
			String rolesString = rs.getString("roleId");
			
			if( rolesString==null || rolesString.equals(""))
				return null;
			String[] rolesStr = rolesString.split(",");
			//System.out.println("|"+rolesString+"|");
			
			for (int i = 0 ; i<rolesStr.length ;i++ ) {
				role =new RolesDao().selectPrecise(Integer.parseInt(rolesStr[i]));
				Roleslist.add(role);
			}
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			util.closeReslutSet(rs);
			util.closePreparedStetement(ps);
			util.closeConnection(conn);
		}
		return Roleslist;
	}
	
	public static void main(String []args){
		Account account = new Account();
		account.setUserId(1);
		for (Roles role : new AccountRolesDao().select(account)) {
			//System.out.println(role.getRoleId()+":"+role.getRoleDesc()+":"+role.getRoleName());
			List<MenuTree> menulist = new RolesMenuDao().select(role);
			for(int i = 0 ; i < menulist.size() ; i++){
				
				MenuTree menu = menulist.get(i);
				if(menu.getParentMenu()==null){
					System.out.println(menu.getTitle());
				}else{
					System.out.println(menu.getParentMenu().getTitle()+":"+menu.getTitle());
				}
			}
		}
		
	}

}
