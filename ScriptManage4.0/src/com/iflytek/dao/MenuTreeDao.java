package com.iflytek.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.iflytek.model.MenuTree;
import com.iflytek.model.Roles;
import com.iflytek.util.Util;

public class MenuTreeDao {


	Connection conn = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	
	public MenuTree selectPrecise(int menuId) {
		MenuTree menuTree = null;	
		Util util = new Util();
		conn = util.getConncetion();
		String sql = "select * from menuTree where treeId="+menuId;
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			rs.next();
			menuTree = new MenuTree();
			menuTree.setTreeId(menuId);
			menuTree.setTitle(rs.getString("title"));
			if(rs.getString("parentId")==null || rs.getString("parentId").equals(""))
				menuTree.setParentMenu(null);
			else
				menuTree.setParentMenu(new MenuTreeDao().selectPrecise(rs.getInt("parentId")));
			
			menuTree.setDesc(rs.getString("desc"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			util.closeReslutSet(rs);
			util.closePreparedStetement(ps);
			util.closeConnection(conn);
		}
		return menuTree;
	}
	
	public static void main(String []args){
		MenuTree menu = new MenuTreeDao().selectPrecise(4);
		System.out.println(menu.getTitle()+"|"+menu.getParentMenu().getTitle());
	}
}
