package com.iflytek.tools;

import com.iflytek.tools.commandstool.CommandTools;


public class ExecuteTools {

	private String ip;
	private String usr;
	private String psword;
	
	public ExecuteTools(String ip, String usr, String psword) {
		super();
		this.ip = ip;
		this.usr = usr;
		this.psword = psword;
	}

	/*
	 * The readAuthor method of the Tool. 
	 * This method checks whether the user has read permissions
	 * @param dir 
	 * 	the dir is the script in the folder
	 * @param ScriptName 
	 *  the ScriptName is the script's name
	 * */
	public Boolean readAuthor(String dir ,String ScriptName){
		Boolean ret = true;
		CommandTools exec = new CommandTools( this.ip, this.usr , this.psword );
		
		try {
			String author = exec.exec("ls -al "+dir+"/"+ScriptName+" | awk '{print $1}'");
			int first = author.indexOf("r");
			if( first==-1 | first != 1){
				ret = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}


	/*
	 * The writeAuthor method of the Tool. 
	 * This writeAuthor checks whether the user has write permissions
	 * @param dir 
	 * 	the dir is the script in the folder
	 * @param ScriptName 
	 *  the ScriptName is the script's name
	 * */
	public Boolean writeAuthor(String dir ,String ScriptName){
		Boolean ret = true;
		CommandTools exec = new CommandTools( this.ip, this.usr , this.psword );
		
		try {
			String author = exec.exec("ls -al "+dir+"/"+ScriptName+" | awk '{print $1}'");
			int first = author.indexOf("w");
			if( first==-1 | first != 2){
				ret = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	/*
	 * The executeAuthor method of the Tool. 
	 * This executeAuthor checks whether the user has execute permissions
	 * @param dir 
	 * 	the dir is the script in the folder
	 * @param ScriptName 
	 *  the ScriptName is the script's name
	 * */
	public Boolean executeAuthor(String dir ,String ScriptName){
		Boolean ret = true;
		CommandTools exec = new CommandTools( this.ip, this.usr , this.psword );
		
		try {
			String author = exec.exec("ls -al "+dir+"/"+ScriptName+" | awk '{print $1}'");
			int first = author.indexOf("x");
			if( first==-1 | first != 3){
				ret = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}
	
	/*
	 * The touchScript method of the Tool. 
	 * This touchScript create script
	 * @param dir 
	 * 	the dir is the script in the folder
	 * @param ScriptName 
	 *  the ScriptName is the script's name
	 * @param type 
	 *  the type is the script's type
	 *  @param content 
	 *  the content is the script's content
	 * */
	public Boolean touchScript(String dir , String ScriptName , int type , String content){
		
		Boolean ret = true ;
		CommandTools exec = new CommandTools( this.ip, this.usr , this.psword );
		try {
			
			String result = exec.exec("echo "+ content+" > "+dir+"/"+ScriptName+"1" );
			System.out.println(result);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	/*
	 * The scpScript method of the Tool. 
	 * This scpScript send script to server
	 * @param dir 
	 * 	the dir is the script in the folder
	 * @param ScriptName 
	 *  the ScriptName is the script's name
	 * @param ip
	 * 	server's ip 
	 * @param account 
	 *  user's name
	 * @param passwd
	 * 	user's password
	 * 
	 * this Method is deprecated.
	 * */

	public String scpScript(String ip ,String account ,String passwd,String dir , String scriptName){
		String ret = "" ;
		CommandTools exec = new CommandTools( this.ip, this.usr , this.psword );
		try {
			
			ret = exec.exec("scp /home/cqaupdate/apache-tomcat-7.0.55/"+scriptName+" "+account+"@"+ip+":"+dir+scriptName);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	/*
	 * The catScript method of the Tool. 
	 * This catScript get the script's content
	 * @param dir 
	 * 	the dir is the script in the folder
	 * @param ScriptName 
	 *  the ScriptName is the script's name
	 * @param type 
	 *  the type is the script's type
	 * */
	@SuppressWarnings("finally")
	public String catScript(String dir , String scriptName ,int type){
		String ret="";
		CommandTools exec = new CommandTools( this.ip, this.usr , this.psword );
		try {
			//System.out.println(readAuthor(dir, scriptName));
			if( readAuthor(dir, scriptName) && type != 4){
				ret = exec.readScript(dir,scriptName);
				//System.out.println(ret);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			return ret;
		}
	}

	/*
	 * The executeScript method of the Tool. 
	 * This executeScript execute the script on server that The script is located in the server
	 * @param dir 
	 * 	the dir is the script in the folder
	 * @param ScriptName 
	 *  the ScriptName is the script's name
	 * @param type 
	 *  the type is the script's type
	 * @param param 
	 *  the param is that Running the scri
	 * */
	public String executeScript(String dir , String ScriptName , int type , String param){
		
		String ret = "" ;

		CommandTools exec = new CommandTools( this.ip, this.usr , this.psword );
		try {
			if(executeAuthor(dir, ScriptName)){
				if( type == 1 ){
					ret = exec.exec("python "+dir+"/"+ScriptName+" "+param);
					
				}else if( type == 2 ){
					ret = exec.exec(dir+"/"+ScriptName+" "+param);
					//System.out.println(dir+"/"+ScriptName+" "+param);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}
	
	
	public static void main(String args[]) throws Exception {
		ExecuteTools tools = new ExecuteTools("192.168.86.39", "zfwu", "zfwu");
		System.out.println(tools.executeScript("/home/zfwu/ScriptManage","test.py",1 ,""));

	}
}
