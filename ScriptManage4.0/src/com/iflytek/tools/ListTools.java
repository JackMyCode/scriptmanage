package com.iflytek.tools;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import com.iflytek.model.MenuTree;

public class ListTools<T> {
	List<T> list = null;

	public ListTools(List<T> list) {
		this.list = list;
	}
	
	public void removeDuplicate(){
		HashSet h  =   new  HashSet(list); 
	    list.clear(); 
	    list.addAll(h); 
	    System.out.println(list); 
	}
	
	public static void main(String []args){
		MenuTree menu1 = new MenuTree();
		menu1.setTreeId(1);
		
		MenuTree menu2 = new MenuTree();
		menu2.setTreeId(2);
		
		MenuTree menu3 = new MenuTree();
		menu3.setTreeId(1);
		
		MenuTree menu4 = new MenuTree();
		menu4.setTreeId(3);
		
		MenuTree menu5 = new MenuTree();
		menu5.setTreeId(1);
		
		MenuTree menu6 = new MenuTree();
		menu6.setTreeId(1);
		
		List<MenuTree> menulist = new ArrayList<MenuTree>();
		menulist.add(menu1);
		menulist.add(menu2);
		menulist.add(menu3);
		menulist.add(menu4);
		menulist.add(menu5);
		menulist.add(menu6);
		
		ListTools<MenuTree> listtools = new ListTools<MenuTree>(menulist);
		listtools.removeDuplicate();
		for(MenuTree mentree:listtools.list){
			System.out.print(mentree.getTreeId()+":");
		}
	} 
}
