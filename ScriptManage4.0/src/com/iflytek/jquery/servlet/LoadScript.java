package com.iflytek.jquery.servlet;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.iflytek.util.Config;

public class LoadScript extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		boolean flag = ServletFileUpload.isMultipartContent(request);
        FileOutputStream fos = null;
        InputStream is =null;
        
        try {
            if (flag) {
                ServletFileUpload upload = new ServletFileUpload();
                FileItemIterator iter = upload.getItemIterator(request);
                
                while (iter.hasNext()){
                    FileItemStream fis = iter.next();
                    is = fis.openStream();
                    if(fis.isFormField()){
                       // System.out.print(fis.getFieldName());
                        //System.out.println(":"+ Streams.asString(is));
                    }else{
                        System.out.println(fis.getName());
                       // String path = request.getSession().getServletContext().getRealPath("/upload");
                        String path = Config.localTestDir + fis.getName();
                        System.out.println(path);
                        fos = new FileOutputStream(path);
                        byte[] buff = new byte[1024];
                        int len = 0;
                        while ((len=is.read(buff))>0){
                            fos.write(buff,0,len);
                            
                        }
                    }
                }
            }
        }catch (Exception e){
        }
        
        /*String account = getParameterValue(request,"account");
		String dir = getParameterValue(request,"dir");
		System.out.println(scriptName);
		 Account user = new AccountDao().selectPrecise(new Account(Integer.parseInt(account)));
	     String param = Config.serverDir + scriptName +" "+ user.getAccount()+" "+user.getServer().getIp()+" "+dir+" "+user.getPasswd() ;
		// ExecuteTools executeTools = new ExecuteTools( Config.ip, Config.serverUser, Config.serverPasswd );
		// String str = executeTools.executeScript(Config.scpDir, Config.scpScript, 2,param);
		System.out.println(param);*/
	}
	
	
	/** 
     *获取各种类型表单的表单参数 
     *@paramrequest HttpServletRequest请求对像 
     * @paramparamName 参数名 
     *@return 
	 * @throws UnsupportedEncodingException 
     *@throwsFileUploadException 
     */ 
    public static String getParameterValue(HttpServletRequest request,String paramName) throws FileUploadException, UnsupportedEncodingException{ 
	    boolean isMultipart = ServletFileUpload.isMultipartContent(request); 
	    if(isMultipart==true){ 
	        FileItemFactory factory = new DiskFileItemFactory(); 
	        ServletFileUpload upload = new ServletFileUpload(factory); 
	        List fileItemList = upload.parseRequest(request); 
	        if(fileItemList!=null){ 
	           for(Iterator itr=fileItemList.iterator();itr.hasNext();){ 
	               FileItem fileItem = (FileItem)itr.next(); 
	               if(fileItem.getFieldName().equalsIgnoreCase(paramName)){ 
	                   return new String(fileItem.getString().getBytes("Utf-8"));//中文转码 
	               } 
	           } 
	        } 
	    }else{ 
	        return new String(request.getParameter(paramName).getBytes("Utf-8"));//中文转码 
	    } 
	    return""; 
    } 

}
