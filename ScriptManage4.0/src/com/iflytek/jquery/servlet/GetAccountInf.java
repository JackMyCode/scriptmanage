package com.iflytek.jquery.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONSerializer;

import com.iflytek.dao.AccountDao;
import com.iflytek.dao.ServerDao;
import com.iflytek.model.Account;
import com.iflytek.model.Server;

public class GetAccountInf extends HttpServlet {

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("utf-8");
		response.setContentType("application/json;charset=utf-8");
		
		PrintWriter out = response.getWriter();
		
		int server =  Integer.parseInt( request.getParameter("serverId") );
		
		ArrayList<Account> accountList = (ArrayList<Account>) new AccountDao().selectAccount(server);
		JSONArray jsarry=new JSONArray();
		String accountStr = JSONSerializer.toJSON(accountList).toString();
		
	    jsarry=JSONArray.fromObject(accountList);  
	   
	    out.write(accountStr);
		
		
	}

}
