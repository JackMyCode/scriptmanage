package com.iflytek.jquery.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.iflytek.dao.ScriptDao;
import com.iflytek.model.Script;
import com.iflytek.tools.ExecuteTools;

public class ExecuteScript extends HttpServlet {

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
      
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		
		ScriptDao scriptDao = new ScriptDao();
		
		long scriptId = Integer.parseInt(request.getParameter("scriptId"));
		
		System.out.println(scriptId);
		Script script = new Script();
		script.setScriptId(scriptId);
		
		Script scriptRun = (Script) scriptDao.selectPrecise(script);
		
		ExecuteTools tools = new ExecuteTools(scriptRun.getServer().getIp(), scriptRun.getUser().getAccount(), scriptRun.getUser().getPasswd());
		String ret = tools.executeScript(scriptRun.getDir(), scriptRun.getScriptName(), scriptRun.getType(),"");
		PrintWriter out = response.getWriter();
		out.write(ret);
		System.out.println(ret);
	}
}
