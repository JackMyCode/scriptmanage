package com.iflytek.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.iflytek.dao.ScriptDao;
import com.iflytek.model.Script;

public class SearchScript extends HttpServlet {

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		
		String serverip = request.getParameter("ip");
		String scriptName = request.getParameter("scriptName");
		
		System.out.println(serverip+"|"+scriptName+"|");
		
		
		
		ScriptDao scriptDao = new ScriptDao();
		
		ArrayList<Script> scriptList = (ArrayList) scriptDao.select(serverip, scriptName);
		
		request.getSession().setAttribute("list", scriptList);
		
		request.setAttribute("list", scriptList);
		request.getRequestDispatcher("/index.jsp").forward(request,
				response);
	}

}
