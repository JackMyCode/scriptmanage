#!/bin/bash
expect -c "
        set timeout 1200;
        spawn /usr/bin/scp -r /home/zfwu/$1 $2@$3:$4
        expect {
                \"*yes/no*\" {send \"yes\r\"; exp_continue}
                \"*password*\" {send \"$5\r\";}   
        }
expect eof;"
